import React, { useContext} from "react"
import axios from "axios"
import "./List.css"
import { DataBuahContext } from "./dataBuahContext"


const BuahForm = () =>{

    
    const[buahBuahan, setBuahBuahan, inputBuah, setInput, selectedId, setSelectedId, statusForm, setStatusForm] = useContext(DataBuahContext)
    

    const handleChangeName = (event) => {
        const value = event.target.value;
        setInput({
        ...inputBuah,
        [event.target.name]: value
        });
    }
    

    const handleSubmit= (event) => {
        event.preventDefault();

        let name = inputBuah.name;
        let price = inputBuah.price;
        let weight = inputBuah.weight;

        console.log(name);
        if(name.replace(/\s/g,"") !== ""){
        if(statusForm === "create"){
            axios.post(`http://backendexample.sanbercloud.com/api/fruits`,{name,price,weight})
            .then(res => {
                setBuahBuahan([...buahBuahan, {id: res.data.id, name: res.data.name, price: res.data.price, weight: res.data.weight}])
            })
        }else if(statusForm === "edit"){
            axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`)
            .then(res => {
                let newBuahBuahan = buahBuahan.find(el=> el.id === selectedId)
                newBuahBuahan.name = name;
                newBuahBuahan.price = price;
                newBuahBuahan.weight = weight;
                setBuahBuahan([...buahBuahan])
            })
            }

            setStatusForm("create");
            setSelectedId(0)
            setInput("")
        }
    }

    return (
        <>
        <h1>Form Beli Buah</h1>
        <form onSubmit={handleSubmit}>
            <br />
            <label>Nama :</label>
            <input type="text" name="name" value={inputBuah.name} onChange={handleChangeName} /><br />
            <label>Harga buah :</label>
            <input type="text" name="price" value={inputBuah.price} onChange={handleChangeName}  /><br />
            <label>Berat Buah :</label>
            <input type="text" name="weight" value={inputBuah.weight} onChange={handleChangeName} /><br />
            <button>submit</button>
        </form>
        </>
    )
}

export default BuahForm;