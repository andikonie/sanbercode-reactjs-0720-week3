import React, { useEffect, useContext} from "react"
import axios from "axios"
import "./List.css"
import { DataBuahContext } from "./dataBuahContext"
import BuahAction from "./BuahAction"
import BuahForm from "./BuahForm"

const DataBuah = () => {
  const[buahBuahan, setBuahBuahan] = useContext(DataBuahContext)
  

  useEffect( () => {
    if(buahBuahan === null){
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        console.log(res.data)
        setBuahBuahan(res.data.map(el => { return {id: el.id, name: el.name, price: el.price, weight: el.weight}} ))
       
      })
    }
  })



  const TableHeader = () => {
    return (
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Harga</th>
          <th>Berat</th>
          <th>Aksi</th>
        </tr>
      </thead>
    )
  }

    return (
     
        <>
        <div>
          <table>
            <TableHeader />
            <tbody>
              {buahBuahan !== null && buahBuahan.map((row, index ) => {
                return(
                  <tr key={index}>
                    <td>{row.id}</td>
                    <td>{row.name}</td>
                    <td>{row.price}</td>
                    <td>{row.weight / 1000} kg</td>
                      <BuahAction id= {row.id} />
                  </tr>
                )
              })}
            </tbody>
            
          </table>

        </div>
      <BuahForm />
      </>
    );
  


}

export default DataBuah;
