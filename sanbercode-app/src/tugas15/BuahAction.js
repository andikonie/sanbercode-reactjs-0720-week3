import React, {useState, useContext} from "react"
import axios from "axios"
import "./List.css"
import { DataBuahContext } from "./dataBuahContext"

const BuahAction = (props) => {

    const [buahBuahan, setBuahBuahan, inputBuah, setInput, selectedId, setSelectedId, statusForm, setStatusForm ] = useContext(DataBuahContext)
    //const [selectedId, setSelectedId]  =  useState(props.id)
    setSelectedId(props.id);

    const handleDelete = (event) => {  
        let idBuah = parseInt(event.target.value);
        let newListBuah = buahBuahan.filter(el => el.id !== idBuah);
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
         .then(res => {
           console.log(res)
         })
    
         setBuahBuahan([...newListBuah])
      }
    
      const handleEdit = (event) =>{
        let idBuah = parseInt(event.target.value);
        let buahSelected = buahBuahan.find(x=> x.id === idBuah)
        setInput({
          name: buahSelected.name,
          price: buahSelected.price,
          weight: buahSelected.weight
        })
        setSelectedId(idBuah)
        setStatusForm("edit")
      }

      return(
          
        <td>
        <button onClick={handleEdit} value={selectedId}>Edit</button>

        <button onClick={handleDelete} value={selectedId}>Delete</button>
        </td>
      )
}

export default BuahAction;