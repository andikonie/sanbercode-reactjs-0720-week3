import React, { useState, createContext } from "react";

export const DataBuahContext = createContext();

export const DataBuahProvider = props => {
    const [buahBuahan, setBuahBuahan] =  useState(null)
    const [inputBuah, setInput]  =  useState({
        name: "",
        price: "",
        weight: ""
    })
    const [selectedId, setSelectedId]  =  useState(0)
    const [statusForm, setStatusForm]  =  useState("create")

  return (
    <DataBuahContext.Provider value={[buahBuahan, setBuahBuahan, inputBuah, setInput, selectedId, setSelectedId, statusForm, setStatusForm]}>
      {props.children}
    </DataBuahContext.Provider>
  );
};