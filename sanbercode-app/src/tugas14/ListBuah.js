import React, {useState, useEffect} from "react"
import axios from "axios"
import "./List.css"

const ListBuah = () => {
  const [buahBuahan, setBuahBuahan] =  useState(null)
  const [inputBuah, setInput]  =  useState({
    name: "",
    price: "",
    weight: ""
  })
  const [selectedId, setSelectedId]  =  useState(0)
  const [statusForm, setStatusForm]  =  useState("create")

  useEffect( () => {
    if(buahBuahan === null){
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        console.log(res.data)
        setBuahBuahan(res.data.map(el => { return {id: el.id, name: el.name, price: el.price, weight: el.weight}} ))
       
      })
    }
  }, [buahBuahan])

  const handleDelete = (event) => {
    let idBuah = parseInt(event.target.value);
    let newListBuah = buahBuahan.filter(el => el.id !== idBuah);
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
     .then(res => {
       console.log(res)
     })

     setBuahBuahan([...newListBuah])
  }

  const handleEdit = (event) =>{
    let idBuah = parseInt(event.target.value);
    let buahSelected = buahBuahan.find(x=> x.id === idBuah)
    setInput({
      name: buahSelected.name,
      price: buahSelected.price,
      weight: buahSelected.weight
    })
    setSelectedId(idBuah);
    setStatusForm("edit");
  }

  const handleChangeName = (event) => {
    const value = event.target.value;
    setInput({
      ...inputBuah,
      [event.target.name]: value
    });
  }
  

  const handleSubmit= (event) => {
    event.preventDefault();

    let name = inputBuah.name;
    let price = inputBuah.price;
    let weight = inputBuah.weight;

    console.log(name);
    if(name.replace(/\s/g,"") !== ""){
      if(statusForm === "create"){
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`,{name,price,weight})
         .then(res => {
            setBuahBuahan([...buahBuahan, {id: res.data.id, name: res.data.name, price: res.data.price, weight: res.data.weight}])
         })
      }else if(statusForm === "edit"){
        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`)
         .then(res => {
            let newBuahBuahan = buahBuahan.find(el=> el.id === selectedId)
            newBuahBuahan.name = name;
            newBuahBuahan.price = price;
            newBuahBuahan.weight = weight;
            setBuahBuahan([...buahBuahan])
         })
        }

        setStatusForm("create");
        setSelectedId(0)
        setInput("")
    }
  }

  const TableHeader = () => {
    return (
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Harga</th>
          <th>Berat</th>
          <th>Aksi</th>
        </tr>
      </thead>
    )
  }

    return (
      <>
        <div>
          <table>
            <TableHeader />
            <tbody>
              {buahBuahan !== null && buahBuahan.map((row, index ) => {
                return(
                  <tr key={index}>
                    <td>{row.id}</td>
                    <td>{row.name}</td>
                    <td>{row.price}</td>
                    <td>{row.weight / 1000} kg</td>
                    <td>
                      <button onClick={handleEdit} value={row.id}>Edit</button>

                      <button onClick={handleDelete} value={row.id}>Delete</button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
        <br />
        <h1>Form Beli Buah</h1>
        <br />
          <form onSubmit={handleSubmit}>
            <br />
            <label>Nama :</label>
            <input type="text" name="name" value={inputBuah.name} onChange={handleChangeName} /><br />
            <label>Harga buah :</label>
            <input type="text" name="price" value={inputBuah.price} onChange={handleChangeName}  /><br />
            <label>Berat Buah :</label>
            <input type="text" name="weight" value={inputBuah.weight} onChange={handleChangeName} /><br />
            <button>submit</button>
          </form>
          
      </>
    );
  


}

export default ListBuah;
