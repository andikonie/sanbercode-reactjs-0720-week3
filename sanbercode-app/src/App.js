//import './App.css';
import React, { Component } from 'react'
// import TabelBuah from './tugas11/TabelBuah';
// import Timer from './tugas12/Timer';
import ListBuah from './tugas14/ListBuah';
import { DataBuahContext, DataBuahProvider } from './tugas15/dataBuahContext';
import DataBuah from './tugas15/DataBuah';

class App extends Component {
  render() {
    // let dataHargaBuah = [
    //   {nama: "Semangka", harga: 10000, berat: 1000},
    //   {nama: "Anggur", harga: 40000, berat: 500},
    //   {nama: "Strawberry", harga: 30000, berat: 400},
    //   {nama: "Jeruk", harga: 30000, berat: 1000},
    //   {nama: "Mangga", harga: 30000, berat: 500}
    // ]
    
    return ( 
      <>
     
      <h1>Tabel Harga Buah</h1>
       {/* Tugas 11-13 :  */}
      {/* <div> <ListBuah />   </div> */}
      {/* Tugas 12 : */}
      {/* <div>  <Timer time={100} />  </div> */}
      
      {/* tugas 14: */}
      {/* <ListBuah /> */}

      {/* tugas 15 */}
      <DataBuahProvider>
      <DataBuah />
      </DataBuahProvider>
      </>
    )
  }
}

export default App
