import React, { Component } from "react";
import "./List.css"

class ListBuah extends Component {
   constructor(props){
       super(props);
       this.state = {
        buahBuahan: [
          {nama: "Semangka", harga: 10000, berat: 1000},
          {nama: "Anggur", harga: 40000, berat: 500},
          {nama: "Strawberry", harga: 30000, berat: 400},
          {nama: "Jeruk", harga: 30000, berat: 1000},
          {nama: "Mangga", harga: 30000, berat: 500}
        ],
        input:{
          nama: "",
          harga: "",
          berat: ""
        },
        indexOfForm: -1
       };
       console.log(this.state.buahBuahan)

       this.handleChange = this.handleChange.bind(this);
       this.handleSubmit = this.handleSubmit.bind(this);
       this.handleEdit = this.handleEdit.bind(this);
       this.handleDelete = this.handleDelete.bind(this);

  }
  handleChange(event){
    let input = {...this.state.input}
    input[event.target.name] = event.target.value
    this.setState({
      input
    })
  }

  handleSubmit(event){
    event.preventDefault();

    let input = this.state.input;

    if(input["nama"].replace(/\s/g,"") !== "" && input["harga"].toString().replace(/\s/g,"") !== "" && input["berat"].toString().replace(/\s/g,"")){
      let newListBuah = this.state.buahBuahan;
      let index = this.state.indexOfForm; 
      console.log(index);
      if(index === -1){
        newListBuah = [...newListBuah, input]
      }else{
        newListBuah[index] = input
      }
      this.setState({
        buahBuahan: newListBuah,
        input:{
          nama: "",
          harga: "",
          berat: ""
        },
        indexOfForm: -1
      })
    }
  }

  handleEdit(event){
    let index = event.target.value;
    let buahEdit = this.state.buahBuahan[index];
    this.setState({
      input: {
      nama: buahEdit.nama,
      harga: buahEdit.harga,
      berat: buahEdit.berat
    },
    indexOfForm: index
    })
  }

  handleDelete(event){
    let index = event.target.value;
    let newListBuah = this.state.buahBuahan;
    let editedListBuah = newListBuah[this.state.indexOfForm];
    newListBuah.splice(index,1)

    if(editedListBuah !== undefined){
      var newIndex = newListBuah.findIndex((el) => el === editedListBuah)
      this.setState({buahBuahan: newListBuah, indexOfForm: newIndex})
    }else{
      this.setState({buahBuahan: newListBuah})
    }
  }


  render() {
    
    return (
      <>
        <div>
          <table>
            <TableHeader />
            <tbody>
              {this.state.buahBuahan.map((row, index ) => {
                return(
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{row.nama}</td>
                    <td>{row.harga}</td>
                    <td>{row.berat / 1000} kg</td>
                    <td>
                      <button onClick={this.handleEdit} value={index}>Edit</button>

                      <button onClick={this.handleDelete} value={index}>Delete</button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
        <br />
        <h1>Form Beli Buah</h1>
        <br />
          <form onSubmit={this.handleSubmit}>
            <br />
            <label>Nama :</label>
            <input type="text" name="nama" value={this.state.input.nama} onChange={this.handleChange} /><br />
            <label>Harga buah :</label>
            <input type="text" name="harga" value={this.state.input.harga} onChange={this.handleChange}  /><br />
            <label>Berat Buah :</label>
            <input type="text" name="berat" value={this.state.input.berat} onChange={this.handleChange} /><br />
            <button>submit</button>
          </form>
          
      </>
    );
  }
}


const TableHeader = () => {
  return (
    <thead>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Harga</th>
        <th>Berat</th>
        <th>Aksi</th>
      </tr>
    </thead>
  );
};




export default ListBuah;