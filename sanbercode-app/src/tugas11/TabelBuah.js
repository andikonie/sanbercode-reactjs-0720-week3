import React, { Component } from 'react';

const TableHeader = () => {
  return (
    <thead>
      <tr>
        <th>Nama</th>
        <th>Harga</th>
        <th>Berat</th>
      </tr>
    </thead>
  )
}

const TableBody = props => {
  const rows = props.dataBuah.map((row, index) => {
    return (
      <tr key={index}>
        <td>{row.nama}</td>
        <td>{row.harga}</td>
        <td>{row.berat/1000} kg</td>
      </tr>
    )
  })
  
  return (
    <tbody>{rows}</tbody>
  )
}

class TabelBuah extends Component {
  render() {
    const { dataBuah } = this.props
    
    return (
      <table>
        <TableHeader />
        <TableBody dataBuah={dataBuah} />
      </table>
    )
  }
}

export default TabelBuah